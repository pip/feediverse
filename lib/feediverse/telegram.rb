require 'telegram/bot'
require 'telegram/bot/types'
require 'yaml'
require 'mustache'
require_relative 'chat'

# Todas las respuestas son por tipos 
Telegram::Bot::Client.typed_response!

module Feediverse
  class Telegram < Model
    attribute :username
    index :username
    unique :username

    set :chats, 'Feediverse::Chat'

    attr_reader :client

    # TODO reemplazar por plantilla configurables
    TEMPLATE = "[{{{ title }}}]({{ url }})\n\n{{{ summary }}}".freeze

    def update_chats
      client.get_updates.each do |u|
        # Solo queremos los canales?
        next unless u.channel_post

        name = u.channel_post.chat.id.to_s
        next unless chats.find(name: name).to_a.empty?

        chats << Chat.create(name: name)
      end
    end

    def register_client(token)
      @client ||= ::Telegram::Bot::Client.new(token, username)
    end

    def post(token, post)
      register_client(token)
      message = render(post.attributes)
      update_chats

      chats.each do |chat|
        client.send_message chat_id: chat.name, text: message, parse_mode: 'Markdown'
      end
    end

    def render(data)
      Mustache.render(TEMPLATE, data)
    end
  end
end
